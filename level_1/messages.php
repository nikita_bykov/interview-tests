<?php

class Message
{
    private string $message;

    public function __construct(string $message)
    {
        $this->message = $message;
    }

    public function __toString()
    {
        return $this->message;
    }
}

class FacebookMessageSender
{
    /**
     * @param Message $message
     * @throws Exception
     */
    public function send(Message $message): void
    {
        // ...
    }
}

class SmsMessageSender
{
    /**
     * @param Message $message
     * @throws Exception
     */
    public function send(Message $message): void
    {
        // ...
    }
}

class MessageSender
{
    public function send(Message $message): bool
    {
        $smsSender = new SmsMessageSender();
        $facebookSender = new FacebookMessageSender();

        return $smsSender->send($message) && $facebookSender->send($message);
    }
}

class JustDoIt
{
    public function main()
    {
        $message = new Message('Pierre was right when he said that one must believe in the possibility of happiness in order to be happy, and I now believe in it.');
        $messageSender = new MessageSender();
        $messageSender->send($message);

        // ...
        // ^ вот тут была какая-то логика и теперь нам необходимо отправить новое сообщение в Facebook и Telegram

        $message = new Message('There is no greatness where there is not simplicity, goodness, and truth.');
    }
}

